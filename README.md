# Learning Git

Lorem ipsum dolor sit amet!

## Uputstvo za generisanje SSH ključa

```
ssh-keygen -t rsa -b 4096 -C "EMAIL"
```

## Uputstvo za kloniranje

```
git clone git@gitlab.com:bogdanpet/learning-git.git
```